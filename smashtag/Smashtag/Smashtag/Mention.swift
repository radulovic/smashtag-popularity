//
//  Mention.swift
//  Smashtag
//
//  Created by Milan on 10/23/17.
//  Copyright © 2017 Milan Radulovic. All rights reserved.
//

import UIKit
import Twitter
import CoreData

class Mention: NSManagedObject {
    
    class func findOrCreateMention(mention: Mention, with context: NSManagedObjectContext) throws ->  Mention? {
        
        let request: NSFetchRequest<Mention> = Mention.fetchRequest()
        request.predicate = NSPredicate(format: "mention LIKE[cd]%@ && searchTerm LIKE[cd]%@", mention.mention!, mention.searchTerm!)
        
        do {
            let matches = try context.fetch(request)
            if matches.count > 0 {
                matches[0].count += 1
                return matches[0]
            } else {

                addToMentions(mention, context: context)
            }

        } catch {
            throw error
        }
        return nil
    }
    
    static func addToMentions(_ mention: Mention, context: NSManagedObjectContext) {
        
        let entity = NSEntityDescription.entity(forEntityName: "Mention", in: context)!
        let mentionObject = NSManagedObject(entity: entity,  insertInto: context)
        
        mentionObject.setValue(mention.mention, forKeyPath: "mention")
        mentionObject.setValue(mention.searchTerm, forKey: "searchTerm")
        mentionObject.setValue(mention.type, forKey: "type")
        mentionObject.setValue(0, forKey: "count")
        
        do {
            try context.save()
        } catch
            let error as NSError {
                print("Mention could not be saved. \(error), \(error.userInfo)")
        }
    }
}
