//
//  SmashTweetTableViewController.swift
//  Smashtag
//
//  Created by Milan on 10/5/17.
//  Copyright © 2017 Milan Radulovic. All rights reserved.
//

import UIKit
import Twitter
import CoreData

class SmashTweetTableViewController: TweetTableViewController {
    
    // MARK: - Model
    
    private var dataManager = SmashTweetDataManager()
    
    // MARK: - Public API
    
    override func insertTweets(_ newTweets: [Tweet]) {
        super.insertTweets(newTweets)
        dataManager.updateRecentSearches(with: searchText!)
        dataManager.addMentions(newTweets, searchText: searchText!)
        dataManager.updateDatabase(with: newTweets)
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        if segue.identifier == "Tweeters Mentioning Search Term" {
            if let tweetersTVC = segue.destination as? SmashTweetersTableViewController {
                tweetersTVC.mention = searchText
                tweetersTVC.container = dataManager.container
            }
        }
    }
}
