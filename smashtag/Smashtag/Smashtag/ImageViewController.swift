//
//  ImageViewController.swift
//  Smashtag
//
//  Created by Milan Radulovic on 9/13/17.
//  Copyright © 2017 Milan Radulovic. All rights reserved.
//

import UIKit

class ImageViewController: UIViewController, UIScrollViewDelegate {
    
    // MARK: - Properties
    
    var url: URL? {
        didSet {
            if let url = url { getImage(from: url) }
        }
    }
    
    // MARK: - Outlets
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imageView: UIImageView!
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initZoom()
    }
    
    // MARK: - Public API
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.imageView
    }
    
    // MARK: - Private API
    
    private func getImage(from url: URL) {
        
        DispatchQueue.global(qos: .userInitiated).async { [weak self] in
            let urlContents = try? Data(contentsOf: url)
            
            if let imageData = urlContents,
                url == self?.url {
                
                DispatchQueue.main.async {
                    self?.imageView.image = UIImage(data: imageData)
                }
            }
        }
    }
    
    private func initZoom() {
        self.scrollView.minimumZoomScale = 0.5
        self.scrollView.maximumZoomScale = 5
    }
}
