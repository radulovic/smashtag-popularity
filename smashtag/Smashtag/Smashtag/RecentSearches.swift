//
//  RecentSearches.swift
//  Smashtag
//
//  Created by Milan on 10/26/17.
//  Copyright © 2017 Milan Radulovic. All rights reserved.
//

import UIKit
import CoreData

class RecentSearches: NSManagedObject {

    class func findOrCreateRecentSearch(with keyword: String, inContext context: NSManagedObjectContext) throws -> RecentSearches? {
        
        let request: NSFetchRequest<RecentSearches> = RecentSearches.fetchRequest()
        request.predicate = NSPredicate(format: "searchTerm LIKE[cd]%@", keyword)
        
        do {
            let matches = try context.fetch(request)
            if matches.count > 0 {
                return matches[0]
            } else {
                
                addToRecentSearches(searchTerm: keyword, context: context)
            }
            
        } catch {
            throw error
        }
        return nil
    }
    
    static func addToRecentSearches(searchTerm: String, context: NSManagedObjectContext) {
        
        let entity = NSEntityDescription.entity(forEntityName: "RecentSearches", in: context)!
        let recentSearchObject = NSManagedObject(entity: entity,  insertInto: context)
        
        recentSearchObject.setValue(searchTerm, forKeyPath: "searchTerm")
        
        do {
            try context.save()
        } catch
            let error as NSError {
                print("Recent search item could not be saved. \(error), \(error.userInfo)")
        }
    }
}
