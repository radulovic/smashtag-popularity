//
//  Tvit.swift
//  Smashtag
//
//  Created by Milan on 10/5/17.
//  Copyright © 2017 Milan Radulovic. All rights reserved.
//

import UIKit
import Twitter
import CoreData

class Tvit: NSManagedObject {
    
    // MARK: - API
    
    class func findOrCreateTweet(matching twitterInfo: Twitter.Tweet, in context: NSManagedObjectContext) throws -> Tvit {
        
        let request: NSFetchRequest<Tvit> = Tvit.fetchRequest()
        request.predicate = NSPredicate(format: "unique = %@", twitterInfo.identifier)
        
        do {
            let matches = try context.fetch(request)
            if matches.count > 0 {
                assert(matches.count == 1, "Tweet.findOrCreateTweet -- database incosistency!")
                return matches[0]
            }
        } catch {
            throw error
        }
        
        let tweet = Tvit(context: context)
        tweet.unique = twitterInfo.identifier
        tweet.text = twitterInfo.text
        tweet.created = twitterInfo.created
        tweet.tweeter = try? TweeterUser.findOrCreateTweeter(matching: twitterInfo.user, in: context)
        
        return tweet
    }
}
