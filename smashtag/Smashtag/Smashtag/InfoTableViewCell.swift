//
//  InfoTableViewCell.swift
//  Smashtag
//
//  Created by Milan Radulovic on 9/13/17.
//  Copyright © 2017 Milan Radulovic. All rights reserved.
//

import UIKit

class InfoTableViewCell: UITableViewCell {

    // MARK: - Outlets
    
    @IBOutlet weak var imageDisplay: UIImageView!

}
