//
//  InfoTableViewController.swift
//  Smashtag
//
//  Created by Milan Radulovic on 9/12/17.
//  Copyright © 2017 Milan Radulovic. All rights reserved.
//

import UIKit
import Foundation
import Twitter

    // MARK: - Protocols

    protocol TransferDataBetweenViewControllers: class {
        func passData(string: String)
    }

class InfoTableViewController: UITableViewController {
    
    // MARK: - Protocol implementations
    
    weak var delegate: TransferDataBetweenViewControllers?
    
    // MARK: - Properties
    
    var tweet: Tweet? {
        didSet {
            
            countElements()
            
            if let countMedia = tweet?.media.count,
                countMedia > 0 {
                
                createSection(named: sectionNames[0], numOfAlreadyCreatedSections)
            }
            
            if let countHashTags = tweet?.hashtags.count,
                countHashTags > 0 {
                
                createSection(named: sectionNames[1], numOfAlreadyCreatedSections)
            }
            
            if let countUserMentions = tweet?.userMentions.count,
                countUserMentions > 0 {
                
                createSection(named: sectionNames[2], numOfAlreadyCreatedSections)
            }
        }
    }
    
    var imageVC = ImageViewController()
    var numberOfSec = 0
    var numOfAlreadyCreatedSections = 0
    var rowsInMediaSection = 0
    var rowsInHashtagSection = 0
    var rowsInUserMentionSection = 0
    var rowsInURLsSection = 0
    var sectionNames = ["Media", "Hashtags", "User mentions", "References"]
    var numberOfHashtags: Int?
    var numberOfUserMentions: Int?
    var numberOfImages: Int?
    var numberOfURLs: Int?
    
    // MARK: - TableView Data source
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0 {
            return 255.0
        }
        return 50.0
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return numberOfSec
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
            
        case 0:
            return rowsInMediaSection
        case 1:
            return rowsInHashtagSection
        case 2:
            return rowsInUserMentionSection
        case 3:
            return rowsInURLsSection
        default:
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: UITableViewCell
        
        if indexPath.section == 0 {
            
            cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
            
            if let medias = tweet?.media,
                !medias.isEmpty {
                
                DispatchQueue.main.async {
                    let urlContents = try? Data(contentsOf: (medias[0].url))
                    if let imageData = urlContents {
                        (cell as? InfoTableViewCell)?.imageDisplay.image = UIImage(data: imageData)
                    }
                }
            }
            
        } else {
            
            cell = tableView.dequeueReusableCell(withIdentifier: "Cell2", for: indexPath)
            
            switch indexPath.section {
                
            case 1:
                cell.textLabel?.text = tweet?.hashtags[indexPath.row].keyword
                
            case 2:
                cell.textLabel?.text = tweet?.userMentions[indexPath.row].keyword
                
            case 3:
                cell.textLabel?.text = tweet?.urls[indexPath.row].keyword
                
            default:
                break
            }
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if #available(iOS 10.0, *),
            indexPath.section == 3 {
            
            UIApplication.shared.open((tweet?.user.profileImageURL)!, options: [:], completionHandler: nil)
        }
        
        if indexPath.section == 1, let hashTag = tweet?.hashtags[indexPath.row].keyword {
            delegate?.passData(string: hashTag)
            navigationController?.popViewController(animated: true)
        }
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?{
        
        switch section {
            
        case 0:
            if rowsInMediaSection == 0 { return "" }
            return sectionNames[0]
            
        case 1:
            if rowsInHashtagSection == 0 { return "" }
            return sectionNames[1]
            
        case 2:
            if rowsInUserMentionSection == 0 { return "" }
            return sectionNames[2]
            
        case 3:
            if rowsInURLsSection == 0 { return "" }
            return sectionNames[3]
            
        default:
            return ""
        }
    }
    
    // MARK: - Segue
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        var destination = segue.destination
        
        if let navigationController = destination as? UINavigationController {
            destination = navigationController.visibleViewController ?? destination
        }
        
        if let identifier = segue.identifier {
            
            switch identifier {
                
            case "showImage":
                if let imageVC = destination as? ImageViewController {
                    imageVC.url = tweet?.media[0].url
                }
                
            default:
                break
            }
        }
    }
    
    // MARK: - Private API
    
    private func countNeededSectionsForMedia(forObject object: Int? = 0) {
        if let sectionsNeeded = object {
            rowsInMediaSection = sectionsNeeded
        }
    }
    
    private func countNeededSectionsForHashtags(forObject object: Int? = 0) {
        if let sectionsNeeded = object {
            rowsInHashtagSection = sectionsNeeded
        }
    }
    
    private func countNeededSectionsForUserMentions(forObject object: Int? = 0) {
        if let sectionsNeeded = object {
            rowsInUserMentionSection = sectionsNeeded
        }
    }
    
    private func countNeededSectionsForURLs(forObject object: Int? = 0) {
        if let sectionsNeeded = object {
            rowsInURLsSection = sectionsNeeded
        }
    }
    
    private func createSection(named name: String, _ soFar: Int) {
        
        countNeededSectionsForMedia(forObject: tweet?.media.count)
        countNeededSectionsForHashtags(forObject: tweet?.hashtags.count)
        countNeededSectionsForUserMentions(forObject: tweet?.userMentions.count)
        countNeededSectionsForURLs(forObject: tweet?.urls.count)
        
        numberOfSec += 1
    }
    
    private func countElements() {
        
        numberOfImages = tweet?.media.count ?? 0
        if numberOfImages! > 0 { numberOfSec += 1 }
        
        numberOfHashtags = tweet?.hashtags.count ?? 0
        if numberOfHashtags! > 0 { numberOfSec += 1 }
        
        numberOfUserMentions = tweet?.userMentions.count ?? 0
        if numberOfUserMentions! > 0 { numberOfSec += 1 }
        
        numberOfURLs = tweet?.urls.count ?? 0
        if numberOfURLs! > 0 { numberOfSec += 1 }
    }
}
