//
//  TweeterUser.swift
//  Smashtag
//
//  Created by Milan on 10/5/17.
//  Copyright © 2017 Milan Radulovic. All rights reserved.
//

import UIKit
import CoreData
import Twitter

class TweeterUser: NSManagedObject {
    
    // MARK: - API
    
    class func findOrCreateTweeter(matching twitterInfo: Twitter.User, in context: NSManagedObjectContext) throws -> TweeterUser {
        
        let request: NSFetchRequest<TweeterUser> = TweeterUser.fetchRequest()
        request.predicate = NSPredicate(format: "handle = %@", twitterInfo.screenName)
        
        do {
            let matches = try context.fetch(request)
            if matches.count > 0 {
                assert(matches.count == 1, "TweeterUser.findOrCreateTweeter -- database incosistency!")
                return matches[0]
            }
        } catch {
            throw error
        }
        
        let twitterUser = TweeterUser(context: context)
        twitterUser.name = twitterInfo.screenName
        twitterUser.handle = twitterInfo.name
        
        return twitterUser
    }
}
