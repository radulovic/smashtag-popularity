//
//  TweetTableViewController.swift
//  Smashtag
//
//  Created by Milan Radulovic on 9/11/17.
//  Copyright © 2017 Milan Radulovic. All rights reserved.
//

import UIKit
import Twitter

class TweetTableViewController: UITableViewController, UITextFieldDelegate, TransferDataBetweenViewControllers {
    
    // MARK: - Model
    
    private var tweets = [[Tweet]]()
    
    // MARK: - Properties
    
    private var lastTwitterRequest: Twitter.Request?
    
    var searchText: String? {
        didSet {
            
            searchTextField?.text = searchText
            searchTextField?.resignFirstResponder()
            tweets.removeAll()
            tableView.reloadData()
            searchForTweets()
            title = searchText
        }
    }
    
    // MARK: - Outlets
    
    @IBOutlet weak var searchTextField: UITextField! {
        didSet {
            searchTextField.delegate = self
        }
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(showPopularData(notification:)),
                                               name: NSNotification.Name(rawValue: "incomingData"),
                                               object: nil)
        
        tableView.estimatedRowHeight = tableView.rowHeight
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    // MARK: - TableView Data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return tweets.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tweets[section].count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Tweet", for: indexPath)
        let tweet: Twitter.Tweet = tweets[indexPath.section][indexPath.row]
        
        if let tweetCell = cell as? TweetTableViewCell {
            tweetCell.tweet = tweet
        }
        navigationItem.title = tweet.user.name
        
        return cell
    }
    
    // MARK: - Segue
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        var destinationViewController = segue.destination
        
        if let navigationController = destinationViewController as? UINavigationController {
            destinationViewController = navigationController.visibleViewController ?? destinationViewController
        }
        
        if let infoViewController = destinationViewController as? InfoTableViewController {
            infoViewController.delegate = self
            infoViewController.tweet = (sender as? TweetTableViewCell)?.tweet
        }
    }
    
    // MARK: - Public API
    
    @objc func showPopularData(notification: Notification) {
        if let message = notification.userInfo {
            if let message = message["message"] as? String {
                self.searchText = message
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == searchTextField {
            searchText = searchTextField.text
        }
        return true
    }
    
    func insertTweets(_ newTweets: [Twitter.Tweet]) {
        self.tweets.insert(newTweets, at: 0)
        self.tableView.insertSections([0], with: .fade)
    }
    
    func passData(string: String) {
        self.searchText = string
    }
    
    // MARK: - Private API
    
    private func twitterRequest() -> Twitter.Request? {
        
        if let query = searchText, !query.isEmpty {
            return Twitter.Request(search: query, count: 100)
        }
        return nil
    }
    
    private func searchForTweets() {
        
        if let request = twitterRequest() {
            lastTwitterRequest = request
            request.fetchTweets { [weak self] newTweets in
                DispatchQueue.main.async {
                    if request == self?.lastTwitterRequest {
                        self?.insertTweets(newTweets)
                    }
                }
            }
        }
    }
}
