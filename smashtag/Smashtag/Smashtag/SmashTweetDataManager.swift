//
//  SmashTweetDataManager.swift
//  Smashtag
//
//  Created by Milan on 10/25/17.
//  Copyright © 2017 Milan Radulovic. All rights reserved.
//

import UIKit
import Twitter
import CoreData

struct SmashTweetDataManager {
    
    // MARK: - Properties
    
    var context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext
    var container = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer
    let recents = RecentSearchesTableViewController()
    
    // MARK: - Public API
    
    func addToRecents(termToAppend term: String, from searchText: String) {
    
        let entity = NSEntityDescription.entity(forEntityName: "RecentSearches", in: context!)!
        let term = NSManagedObject(entity: entity,  insertInto: context)
        term.setValue(searchText, forKeyPath: "searchTerm")
        
        do {
            try context?.save()
        } catch
            let error as NSError {
                print("Term could not be saved. \(error), \(error.userInfo)")
        }
    }
    
    func addMentions(_ tweets: [Tweet], searchText: String) {
        
            for tweet in tweets {
                createMention(of: tweet, type: "hashtag", inContext: context!, searchText: searchText)
                createMention(of: tweet, type: "userMention", inContext: context!, searchText: searchText)
        }
    }
    
    func updateDatabase(with tweets: [Twitter.Tweet]) {
        (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.performBackgroundTask { context in
            for twitterInfo in tweets {
                _ = try? Tvit.findOrCreateTweet(matching: twitterInfo, in: context)
            }
            try? context.save()
        }
    }
    
    func updateRecentSearches(with keyword: String) {
        
        _ = try? RecentSearches.findOrCreateRecentSearch(with: keyword, inContext: context!)
    }
    
    // MARK: - Private API
    
    private func createMention(of tweet: Tweet, type: String, inContext: NSManagedObjectContext, searchText: String) {
        
        switch type {
            
        case "hashtag":
            
            for hashtag in tweet.hashtags {
                
                let mention = Mention(context: context!)
                mention.mention = hashtag.keyword
                mention.searchTerm = searchText
                mention.type = "hashtag"
                mention.count = 0
                
                _ = try? Mention.findOrCreateMention(mention: mention, with: context!)
            }
            
        case "userMention":
            
            for mentionObject in tweet.userMentions {
                
                let mention = Mention(context: context!)
                mention.mention = mentionObject.keyword
                mention.searchTerm = searchText
                mention.type = "userMention"
                mention.count = 0
                
                _ = try? Mention.findOrCreateMention(mention: mention, with: context!)
            }
            
        default:
            break
        }
    }
}
