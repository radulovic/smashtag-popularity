//
//  TweetTableViewCell.swift
//  Smashtag
//
//  Created by Milan Radulovic on 9/11/17.
//  Copyright © 2017 Milan Radulovic. All rights reserved.
//

import UIKit
import Twitter

class TweetTableViewCell: UITableViewCell {
    
    // MARK: - Outlets
    
    @IBOutlet weak var tweetProfileImageView: UIImageView!
    @IBOutlet weak var tweetUserLabel: UILabel!
    @IBOutlet weak var tweetTextLabel: UILabel!
    @IBOutlet weak var tweetCreatedLabel: UILabel!
    
    // MARK: - Properties
    
    var tweet: Twitter.Tweet? {
        didSet {
            updateUI()
        }
    }
    
    // MARK: - Private API
    
    private func paintMentions(_ atributted: NSMutableAttributedString, range: NSRange, color: UIColor) {
        
        atributted.addAttribute(NSAttributedStringKey.foregroundColor,
                                value: color,
                                range: range)
    }
    
    private func updateUI() {
        
        if let tweet = tweet {
            
            let attributedText = NSMutableAttributedString(string: tweet.text)
            
            for hashtag in tweet.hashtags {
                paintMentions(attributedText, range: hashtag.nsrange, color: UIColor.red)
            }
            
            for mention in tweet.userMentions {
                paintMentions(attributedText, range: mention.nsrange, color: UIColor.orange)
            }
            
            for url in tweet.urls {
                paintMentions(attributedText, range: url.nsrange, color: UIColor.blue)
            }
            
            tweetTextLabel?.attributedText = attributedText
        }
        
        tweetUserLabel?.text = tweet?.user.description
        
        if let profileImageURL = tweet?.user.profileImageURL,
            let imageData = try? Data(contentsOf: profileImageURL) {
            tweetProfileImageView?.image = UIImage(data: imageData)
        } else {
            tweetProfileImageView?.image = nil
        }
        if let created = tweet?.created {
            let formatter = DateFormatter()
            
            if Date().timeIntervalSince(created) > 24*60*60 {
                formatter.dateStyle = .short
            }
            tweetCreatedLabel?.text = formatter.string(from: created)
        } else {
            tweetCreatedLabel?.text = nil
        }
    }
}
