//
//  PopularTableViewController.swift
//  Smashtag
//
//  Created by Milan on 10/23/17.
//  Copyright © 2017 Milan Radulovic. All rights reserved.
//

import UIKit
import CoreData

class PopularTableViewController: FetchedResultsTableViewController {

    // MARK: - Properties
    
    var searchTerm: String? {
        didSet {
            updateUI()
        }
    }
    
    var fetchedResultsController: NSFetchedResultsController<Mention>?
    private var container = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return fetchedResultsController?.sections?.count ?? 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let sections = fetchedResultsController?.sections, sections.count > 0 {
            return sections[section].numberOfObjects
        } else {
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if let sections = fetchedResultsController?.sections, sections.count > 0 {
            return sections[section].name
        } else {
            return nil
        }
    }
    
    override func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return fetchedResultsController?.sectionIndexTitles
    }
    
    override func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int {
        return fetchedResultsController?.section(forSectionIndexTitle: title, at: index) ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        
        if let mention = fetchedResultsController?.object(at: indexPath) {
            cell.textLabel?.text = mention.mention!
            cell.detailTextLabel?.text = ("Number of mentions: \(mention.count)")
        }
        return cell
    }
    
    // MARK: - Private API
    
    private func updateUI() {
        
        if let context = container?.viewContext,
            searchTerm != nil {
            let request: NSFetchRequest<Mention> = Mention.fetchRequest()
            
            request.sortDescriptors = [
                NSSortDescriptor(key: "type", ascending: false),
                NSSortDescriptor(key: "count", ascending: false),
                NSSortDescriptor(key: "mention", ascending: true, selector: #selector(NSString.localizedCaseInsensitiveCompare(_:)))
            ]
            
            request.predicate = NSPredicate(format: "searchTerm contains[cd] %@ AND count > 1", searchTerm!)
            
            fetchedResultsController = NSFetchedResultsController<Mention>(
                fetchRequest: request,
                managedObjectContext: context,
                sectionNameKeyPath: "type",
                cacheName: nil)
        }
        fetchedResultsController?.delegate = self
        
        do {
            try fetchedResultsController?.performFetch()
        } catch {
            fatalError("Couldn't fetch data from the database!")
        }
        
        tableView.reloadData()
    }
}
