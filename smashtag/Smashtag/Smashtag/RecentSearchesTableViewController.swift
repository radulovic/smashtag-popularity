//
//  RecentSearchesTableViewController.swift
//  Smashtag
//
//  Created by Milan on 10/13/17.
//  Copyright © 2017 Milan Radulovic. All rights reserved.
//

import UIKit
import Twitter
import CoreData

class RecentSearchesTableViewController: UITableViewController {

    // MARK: - Properties
    
    var recentSearches: [NSManagedObject] = []
    var filteredTweets: [NSManagedObject] = []
    var numberOfFilteredTweets: Int = 0
    
    // MARK: - Lifecycle
    
    override func viewWillAppear(_ animated: Bool) {
        fetch()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.clearsSelectionOnViewWillAppear = true
        tableView.reloadData()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recentSearches.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        let recent = recentSearches[indexPath.row]
        if let text = recent.value(forKey: "searchTerm") as? String {
            
            countFiltered(with: text)
            
            cell.textLabel?.text = text
            cell.detailTextLabel?.text = String(numberOfFiltered())
        }
        return cell
    }
    
    // MARK: - Private API
    
    private func numberOfFiltered() -> Int {
        return numberOfFilteredTweets
    }
    
    private func countFiltered(with text: String) {
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let context = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Tvit")
        let predicate = NSPredicate(format: "text CONTAINS[c] %@", text)
        fetchRequest.predicate = predicate
        
        do {
            filteredTweets = try context.fetch(fetchRequest)
            numberOfFilteredTweets = filteredTweets.count
        } catch
            let error as NSError {
            print("Could not filter tweets. \(error), \(error.userInfo)")
        }
    }
    
    private func fetch() {
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        
        let context = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "RecentSearches")
        
        do {
            recentSearches = try context.fetch(fetchRequest)
            recentSearches.reverse()
            tableView.reloadData()
        } catch let error as NSError {
            print("Could not fetch recent searches. \(error), \(error.userInfo)")
        }
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        var destinationViewController = segue.destination
        
        if let navigationController = destinationViewController as? UINavigationController {
            destinationViewController = navigationController.visibleViewController ?? destinationViewController
        }
        
        if let popularViewController = destinationViewController as? PopularTableViewController {

            popularViewController.searchTerm = ((sender as? UITableViewCell)?.textLabel?.text)!
        }
    }
}
