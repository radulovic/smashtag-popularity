//
//  PopularMentionsTableViewController.swift
//  Smashtag
//
//  Created by Milan on 10/13/17.
//  Copyright © 2017 Milan Radulovic. All rights reserved.
//

import UIKit
import Twitter
import CoreData

class PopularMentionsTableViewController: UITableViewController {
    
    // MARK: - Model
    
    var tweets: [NSManagedObject] = []
    
    // MARK: - Properties
    
    var mention: String = "" {
        didSet {
            update()
        }
    }
    
    typealias hashtags = (hashtag: String, count: Int)
    typealias userMentions = (mention: String, count: Int)
    
    var arrayOfHashtags = [hashtags]()
    var arrayOfMentions = [userMentions]()
    var context = ""
    var words: [String] = []
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
       
        if arrayOfMentions.isEmpty || arrayOfHashtags.isEmpty {
            return 1
        } else {
            return 2
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
        case 0:
            return arrayOfHashtags.count
        case 1:
            return arrayOfMentions.count
        default:
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "Hashtags"
        case 1:
            return "User mentions"
        default:
            return ""
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        
        switch indexPath.section {
        case 0:
            cell.textLabel?.text = arrayOfHashtags[indexPath.row].hashtag
            cell.detailTextLabel?.text = String(arrayOfHashtags[indexPath.row].count)
        case 1:
            cell.textLabel?.text = arrayOfMentions[indexPath.row].mention
            cell.detailTextLabel?.text = String(arrayOfMentions[indexPath.row].count)
        default:
            cell.textLabel?.text = ""
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var keyword = ""
        
        switch indexPath.section {
            
        case 0:
            keyword = arrayOfHashtags[indexPath.row].hashtag
            NotificationCenter.default.post(name: Notification.Name(rawValue: "incomingData"),
                                            object: nil,
                                            userInfo: ["message" : keyword])
            navigationController?.popViewController(animated: true)
            tabBarController?.selectedIndex = 0
            self.dismiss(animated: true, completion: nil)
            
        case 1:
            keyword = arrayOfMentions[indexPath.row].mention
            NotificationCenter.default.post(name: Notification.Name(rawValue: "incomingData"),
                                            object: nil,
                                            userInfo: ["message" : keyword])
            navigationController?.popViewController(animated: true)
            tabBarController?.selectedIndex = 0
            self.dismiss(animated: true, completion: nil)
            
        default:
            break
        }
    }
    
    // MARK: - Private API
    
    private func update() {
        
        fetch()
        tableView.reloadData()
        getHashtags()
        getUserMentions()
        
        arrayOfHashtags.sort(by: {$0.count > $1.count})
        arrayOfMentions.sort(by: {$0.count > $1.count})
    }
    
    private func fetch() {
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let context = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Tvit")
        let predicate = NSPredicate(format: "text CONTAINS[c] %@", mention)
        fetchRequest.predicate = predicate
        
        do {
            tweets = try context.fetch(fetchRequest)
        } catch
            let error as NSError {
                print("Could not fetch tweets. \(error), \(error.userInfo)")
        }
    }
    
    private func getHashtags() {
        
        for tweet in tweets {
            context = tweet.value(forKey: "text") as! String
            words = context.components(separatedBy: " ")
            
            for word in words {
                if word.hasPrefix("#") {
                    
                    if !checkForDuplicates(term: word) {
                        arrayOfHashtags.append((hashtag: word, count: 1))
                    } else {
                        if let index = arrayOfHashtags.index(where: {$0.hashtag == word}) {
                            arrayOfHashtags[index].count += 1
                        }
                    }
                }
            }
        }
    }
    
    private func checkForDuplicates(term forCheck: String) -> Bool {
        
        if let _ = arrayOfHashtags.index(where: {$0.hashtag == forCheck}) {
            return true
        } else {
            return false
        }
        
    }
    
    private func getUserMentions() {
        
        for tweet in tweets {
            context = tweet.value(forKey: "text") as! String
            words = context.components(separatedBy: " ")
            
            for word in words {
                if word.hasPrefix("@") {
                    
                    if !checkForDuplicates(term: word) {
                        arrayOfMentions.append((mention: word, count: 1))
                    } else {
                        if let index = arrayOfMentions.index(where: {$0.mention == word}) {
                            arrayOfMentions[index].count += 1
                        }
                    }
                }
            }
        }
    }
}
